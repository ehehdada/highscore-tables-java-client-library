# highscore-tables-java-client-library #

This is a library that can be imported in Java projects (J2SE, J2EE, Android, ...) to connect to our Highscore Tables API.

### What is this repository for? ###

* Quick summary

This is the source code for the Java client library for our Highscore Tables API service. 

* Version

Currently it is 2.0 that brings compatibility with Android projects.

### How do I get set up? ###

* Summary of set up

This project has been made using Maven. While it is not yet available in Maven Central Repository, you can clone it from here.

Import it adding in the pom.xml of your project

```xml
<dependency>
	<groupId>com.ehehdada</groupId>
	<artifactId>highscores-client</artifactId>
	<version>2.0</version>
</dependency>
```

* Dependencies
    * json-smart https://code.google.com/p/json-smart/

* How to run tests

In command line, type:

```bash
mvn test
```

* Deployment instructions

In command line, type:

```bash
mvn install
```

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact