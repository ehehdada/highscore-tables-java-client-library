/**
 * 
 */
package com.ehehdada.highscores;

import net.minidev.json.JSONObject;

/**
 * <p>
 * Description:
 * </p>
 * <p>
 * Date: 2014/8/7
 * </p>
 * <p>
 * Copyright (C) 2014 ehehdada, ltd. (<a
 * href="https://www.ehehdada.com">https://www.ehehdada.com</a>) - The ehehdada,
 * ltd. team (<a href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * </p>
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <a
 * href="http://www.gnu.org/licenses/lgpl.html">http
 * ://www.gnu.org/licenses/lgpl.html</a>.
 * </p>
 * 
 * @author The ehehdada, ltd. team (<a
 *         href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * @since 1.0
 */
public class AddEntryResponseContent {
	private Long position;
	private Long entry_limit;
	private Long ip4;
	private Long ip6;

	protected AddEntryResponseContent(Object o) {
		if (o instanceof JSONObject) {
			JSONObject obj = (JSONObject) o;
			Number n;
			n = obj.getAsNumber("position");
			if (n != null) {
				position = n.longValue();
			}
			n = obj.getAsNumber("entry_limit");
			if (n != null) {
				entry_limit = n.longValue();
			}
			n = obj.getAsNumber("ip4");
			if (n != null) {
				ip4 = n.longValue();
			}
			n = obj.getAsNumber("ip6");
			if (n != null) {
				ip6 = n.longValue();
			}
		}
	}

	public Long getPosition() {
		return position;
	}

	public void setPosition(Long position) {
		this.position = position;
	}

	public Long getEntry_limit() {
		return entry_limit;
	}

	public void setEntry_limit(Long entry_limit) {
		this.entry_limit = entry_limit;
	}

	public Long getIp4() {
		return ip4;
	}

	public void setIp4(Long ip4) {
		this.ip4 = ip4;
	}

	public Long getIp6() {
		return ip6;
	}

	public void setIp6(Long ip6) {
		this.ip6 = ip6;
	}

}
