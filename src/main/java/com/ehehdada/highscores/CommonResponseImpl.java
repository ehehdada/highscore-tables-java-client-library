package com.ehehdada.highscores;

import java.io.IOException;

/**
 * 
 * <p>
 * Description:
 * </p>
 * <p>
 * Date: 2014/11/1
 * </p>
 * <p>
 * Copyright (C) 2014 ehehdada, ltd. (<a
 * href="https://www.ehehdada.com">https://www.ehehdada.com</a>) - The ehehdada,
 * ltd. team (<a href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * </p>
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <a
 * href="http://www.gnu.org/licenses/lgpl.html">http
 * ://www.gnu.org/licenses/lgpl.html</a>.
 * </p>
 * 
 * @author The ehehdada, ltd. team (<a
 *         href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * @since 2.0
 */
class CommonResponseImpl implements CommonResponse {
	protected int transportStatusCode;
	protected String transportExtraMessage;
	protected String responseBody;

	public int getTransportStatusCode() throws IOException {
		return transportStatusCode;
	}

	public String getTransportExtraMessage() throws IOException {
		return transportExtraMessage;
	}

	public String getResponseBody() {
		return responseBody;
	}
}