/**
 * 
 */
package com.ehehdada.highscores;

import java.util.Locale;

import net.minidev.json.JSONObject;

/**
 * <p>
 * Description:
 * </p>
 * <p>
 * Date: 2014/7/26
 * </p>
 * <p>
 * Copyright (C) 2014 ehehdada, ltd. (<a
 * href="https://www.ehehdada.com">https://www.ehehdada.com</a>) - The ehehdada,
 * ltd. team (<a href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * </p>
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <a
 * href="http://www.gnu.org/licenses/lgpl.html">http
 * ://www.gnu.org/licenses/lgpl.html</a>.
 * </p>
 * 
 * @author The ehehdada, ltd. team (<a
 *         href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * @since 1.0
 */
public class HighscoreRecord {

	private double score;
	private String by_whom;
	private long when_happened;
	private Locale locale;
	private Long ip4;
	private Long ip6;

	/**
	 * 
	 */
	public HighscoreRecord() {
	}

	/**
	 * @since 2.0
	 * @param o
	 */
	protected HighscoreRecord(Object o) {
		if (o instanceof JSONObject) {
			JSONObject obj = (JSONObject) o;
			score = obj.getAsNumber("score").doubleValue();
			by_whom = obj.getAsString("by_whom");
			when_happened = obj.getAsNumber("when_happened").longValue();
			String s;
			s = obj.getAsString("locale");
			if (s != null) {
				locale = new Locale(s);
			}
			Number n;
			n = obj.getAsNumber("ip4");
			if (n != null) {
				ip4 = n.longValue();
			}
			n = obj.getAsNumber("ip6");
			if (n != null) {
				ip6 = n.longValue();
			}
		}
	}

	public Long getIp4() {
		return ip4;
	}

	public void setIp4(Long ip4) {
		this.ip4 = ip4;
	}

	public Long getIp6() {
		return ip6;
	}

	public void setIp6(Long ip6) {
		this.ip6 = ip6;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public String getBy_whom() {
		return by_whom;
	}

	public void setBy_whom(String by_whom) {
		this.by_whom = by_whom;
	}

	public long getWhen_happened() {
		return when_happened;
	}

	public void setWhen_happened(long when_happened) {
		this.when_happened = when_happened;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

}
