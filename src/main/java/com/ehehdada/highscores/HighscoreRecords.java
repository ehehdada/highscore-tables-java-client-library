/**
 * 
 */
package com.ehehdada.highscores;

import java.util.Vector;

import net.minidev.json.JSONArray;

/**
 * <p>
 * Description:
 * </p>
 * <p>
 * Date: 2014/8/7
 * </p>
 * <p>
 * Copyright (C) 2014 ehehdada, ltd. (<a
 * href="https://www.ehehdada.com">https://www.ehehdada.com</a>) - The ehehdada,
 * ltd. team (<a href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * </p>
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <a
 * href="http://www.gnu.org/licenses/lgpl.html">http
 * ://www.gnu.org/licenses/lgpl.html</a>.
 * </p>
 * 
 * @author The ehehdada, ltd. team (<a
 *         href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * @since 1.0
 */
public class HighscoreRecords extends Vector<HighscoreRecord> {

	/**
	 * @since 2.0
	 * @param o
	 */
	protected HighscoreRecords(Object o) {
		if (o instanceof JSONArray) {
			JSONArray array = (JSONArray) o;
			for (int i = 0, li = array.size(); i < li; i++) {
				addElement(new HighscoreRecord(array.get(i)));
			}
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
