/**
 * 
 */
package com.ehehdada.highscores;

/**
 * 
 * <p>
 * Description: This interface provides the gateway to the different services of
 * high scores table system. All requests are online, and asynchronous. As they
 * are executed in a separated thread, the execution will come you back later
 * through the given implemented listener.
 * </p>
 * <p>
 * Date: 2014/7/24
 * </p>
 * <p>
 * Copyright (C) 2014 ehehdada, ltd. (<a
 * href="https://www.ehehdada.com">https://www.ehehdada.com</a>) - The ehehdada, ltd. team
 *  (<a
 * href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * </p>
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <a
 * href="http://www.gnu.org/licenses/lgpl.html">http
 * ://www.gnu.org/licenses/lgpl.html</a>.
 * </p>
 * 
 * @author The ehehdada, ltd. team  (<a
 *         href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * @since 1.0
 */
public interface HighscoresTable {
	/**
	 * Deletes fully the table
	 * 
	 * @param l
	 *            provide a listener to call back your application
	 * @return an instance of {@link RequestHandle<ResetTableResponse>}
	 * @throws HighscoresTableException
	 *             in case of error
	 */
	RequestHandle<ResetTableResponse> doResetTable(
			ResponseListener<ResetTableResponse> l)
			throws HighscoresTableException;

	/**
	 * Gets the high score table
	 * 
	 * @param l
	 *            provide a listener to call back your application
	 * @return an instance of {@link RequestHandle<GetTableResponse>}
	 * @throws HighscoresTableException
	 *             in case of error
	 */
	RequestHandle<GetTableResponse> doGetTable(
			ResponseListener<GetTableResponse> l)
			throws HighscoresTableException;

	/**
	 * Adds a new record
	 * 
	 * @param hr
	 *            the new record
	 * @param l
	 *            provide a listener to call back your application
	 * @return an instance of {@link RequestHandle<AddEntryResponse>}
	 * @throws HighscoresTableException
	 *             in case of error
	 */
	RequestHandle<AddEntryResponse> doAddEntry(HighscoreRecord hr,
			ResponseListener<AddEntryResponse> l)
			throws HighscoresTableException;

	/**
	 * Checks if the given record can be added to the high score table
	 * 
	 * @param hr
	 *            the new record
	 * @param l
	 *            provide a listener to call back your application
	 * @return an instance of {@link RequestHandle<CheckEntryResponse>}
	 * @throws HighscoresTableException
	 *             in case of error
	 */
	RequestHandle<CheckEntryResponse> doCheckEntry(HighscoreRecord hr,
			ResponseListener<CheckEntryResponse> l)
			throws HighscoresTableException;
}
