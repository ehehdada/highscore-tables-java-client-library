/**
 * 
 */
package com.ehehdada.highscores;

import java.net.URISyntaxException;

/**
 * <p>
 * Description: Use this class to generate instances of {@link HighscoresTable}
 * </p>
 * <p>
 * Date: 2014/7/24
 * </p>
 * <p>
 * Copyright (C) 2014 ehehdada, ltd. (<a
 * href="https://www.ehehdada.com">https://www.ehehdada.com</a>) - The ehehdada, ltd. team
 *  (<a
 * href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * </p>
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <a
 * href="http://www.gnu.org/licenses/lgpl.html">http
 * ://www.gnu.org/licenses/lgpl.html</a>.
 * </p>
 * 
 * @author The ehehdada, ltd. team  (<a
 *         href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * @since 1.0
 */
public class HighscoresTableFactory {

	/**
	 * 
	 */
	private HighscoresTableFactory() {
	}

	/**
	 * Implement {@link Configuration} and provide an instance of it to get an
	 * instance of the implementation of {@link HighscoresTable}
	 * 
	 * @param cfg
	 * @return an instance of {@link HighscoresTable} implementation
	 * @throws URISyntaxException
	 */
	public static HighscoresTable getInstance(final Configuration cfg)
			throws URISyntaxException {
		return new HighscoresTableImpl(cfg);
	}
}
