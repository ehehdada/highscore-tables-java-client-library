package com.ehehdada.highscores;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

/**
 * 
 * <p>
 * Description:
 * </p>
 * <p>
 * Date: 2014/7/24
 * </p>
 * <p>
 * Copyright (C) 2014 ehehdada, ltd. (<a
 * href="https://www.ehehdada.com">https://www.ehehdada.com</a>) - The ehehdada,
 * ltd. team (<a href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * </p>
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <a
 * href="http://www.gnu.org/licenses/lgpl.html">http
 * ://www.gnu.org/licenses/lgpl.html</a>.
 * </p>
 * 
 * @author The ehehdada, ltd. team (<a
 *         href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * @since 1.0
 */
final class HighscoresTableImpl implements HighscoresTable {
	private class RequestHandleImpl<T extends CommonResponse> implements
			RequestHandle<T> {
		private final T response;
		private final Thread thread;
		private boolean cancelled;

		private RequestHandleImpl(T response, Thread thread) {
			this.response = response;
			this.thread = thread;
		}

		public void cancel() {
			thread.interrupt();
			cancelled = true;
		}

		public boolean isCancelled() {
			if (cancelled) {
				return thread.isInterrupted();
			}
			return false;
		}

		public boolean isDone() {
			return !cancelled && !thread.isAlive();
		}

		public HighscoresTable getHighscoresTable() {
			return HighscoresTableImpl.this;
		}

		public T getResponse() {
			if (!isDone()) {
				return null;
			}
			return response;
		}
	}

	private final Configuration cfg;
	private URI uri;

	/**
	 * 
	 * @param cfg
	 * @throws URISyntaxException
	 */
	HighscoresTableImpl(Configuration cfg) throws URISyntaxException {
		this.cfg = cfg;
		uri = new URI("https://highscores.ehehdada.com/api/"
				+ cfg.getTableName());
	}

	/**
	 * 
	 */
	public RequestHandle<ResetTableResponse> doResetTable(
			final ResponseListener<ResetTableResponse> l)
			throws HighscoresTableException {
		final HttpURLConnection huc = prepareConnection("DELETE");
		final ResetTableResponseImpl response = new ResetTableResponseImpl();
		final Thread thread = new Thread() {
			public void run() {
				Throwable throwable;
				try {
					if ((throwable = readResponse(huc, response)) != null) {
						l.onError(HighscoresTableImpl.this, throwable);
					} else {
						try {
							l.onResponse(HighscoresTableImpl.this, response);
						} catch (Throwable t) {
							l.onError(HighscoresTableImpl.this, t);
						}
					}
				} catch (Throwable t) {
					t.printStackTrace(cfg.getPrintStream());
				}
			}
		};
		thread.start();
		return new RequestHandleImpl<ResetTableResponse>(response, thread);
	}

	/**
	 * @since 2.0
	 * @param operation
	 * @return
	 * @throws HighscoresTableException
	 */
	private HttpURLConnection prepareConnection(String operation)
			throws HighscoresTableException {
		return prepareConnection(operation, "");
	}

	/**
	 * 
	 * @since 2.0
	 * @param operation
	 * @param body
	 * @return
	 * @throws HighscoresTableException
	 */
	private HttpURLConnection prepareConnection(String operation, String body)
			throws HighscoresTableException {
		return prepareConnection(operation, body, null, null);
	}

	/**
	 * 
	 * @since 2.0
	 * @param operation
	 * @param body
	 * @param queryParams
	 * @return
	 * @throws HighscoresTableException
	 */
	private HttpURLConnection prepareConnection(String operation, String body,
			String path, HashMap<String, String> queryParams)
			throws HighscoresTableException {
		HttpURLConnection huc;
		try {
			URI uri;
			if (path != null) {
				uri = new URI(this.uri.toString() + path);
			} else {
				uri = this.uri;
			}
			if (queryParams != null) {
				StringBuffer sb = new StringBuffer();
				Iterator<String> keys = queryParams.keySet().iterator();
				for (int i = 0; keys.hasNext(); i++) {
					String key = keys.next();
					if (i > 0) {
						sb.append('&');
					}
					sb.append(urlEncode(key)).append('=')
							.append(urlEncode(queryParams.get(key)));
				}
				uri = new URI(uri.toString() + '?' + sb);
			}
			huc = (HttpURLConnection) uri.toURL().openConnection();
			huc.setRequestMethod(operation);
			huc.setDoInput(true);
			if ("POST".equals(operation)) {
				huc.setDoOutput(true);
			}
			final long currentTimeMillis = System.currentTimeMillis();
			final long timestamp = currentTimeMillis
					- TimeZone.getDefault().getOffset(currentTimeMillis);
			huc.setRequestProperty("timestamp", Long.toString(timestamp));
			huc.setRequestProperty("tablekey", cfg.getTableKey());
			huc.setRequestProperty("fingerprint",
					getFingerprint(body, timestamp));
		} catch (Throwable e) {
			throw new HighscoresTableException("Preparing connection", e);
		}
		return huc;
	}

	/**
	 * 
	 * @param key
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private static String urlEncode(String key)
			throws UnsupportedEncodingException {
		return URLEncoder.encode(key, "utf8");
	}

	/**
	 * 
	 * @param body
	 * @param timestamp
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	private String getFingerprint(String body, long timestamp)
			throws NoSuchAlgorithmException {
		return Utilities.getFingerprint(uri.getPath(), body, timestamp,
				cfg.getTableKey(), cfg.getSharedSecret());
	}

	/**
	 * 
	 */
	public RequestHandle<GetTableResponse> doGetTable(
			final ResponseListener<GetTableResponse> l)
			throws HighscoresTableException {
		final HttpURLConnection huc = prepareConnection("GET");
		final GetTableResponseImpl response = new GetTableResponseImpl();
		final Thread thread = new Thread() {
			public void run() {
				Throwable throwable;
				try {
					if ((throwable = readResponse(huc, response)) != null) {
						l.onError(HighscoresTableImpl.this, throwable);
					} else {
						try {
							if (response.transportStatusCode == HttpURLConnection.HTTP_OK) {
								response.content = new HighscoreRecords(
										JSONValue.parse(response.responseBody));
							}
							l.onResponse(HighscoresTableImpl.this, response);
						} catch (Throwable t) {
							l.onError(HighscoresTableImpl.this, t);
						}
					}
				} catch (Throwable t) {
					t.printStackTrace(cfg.getPrintStream());
				}
			}
		};
		thread.start();
		return new RequestHandleImpl<GetTableResponse>(response, thread);
	}

	/**
	 * @since 2.0
	 * @param is
	 * @return
	 * @throws IOException
	 */
	private static String readFully(InputStream is) throws IOException {
		final char[] ca = new char[8192];
		int nChars;
		final Reader reader = new InputStreamReader(is, "utf8");
		final StringWriter sw = new StringWriter();
		while ((nChars = reader.read(ca)) > 0) {
			sw.write(ca, 0, nChars);
		}
		return sw.toString();
	}

	public RequestHandle<AddEntryResponse> doAddEntry(final HighscoreRecord hr,
			final ResponseListener<AddEntryResponse> l)
			throws HighscoresTableException {
		JSONObject obj = new JSONObject();
		obj.put("score", hr.getScore());
		obj.put("by_whom", hr.getBy_whom());
		obj.put("when_happened", hr.getWhen_happened());
		obj.put("locale", hr.getLocale().toString());
		final String body = obj.toString();
		final HttpURLConnection huc = prepareConnection("POST", body);
		final AddEntryResponseImpl response = new AddEntryResponseImpl();
		final Thread thread = new Thread() {
			public void run() {
				Throwable throwable;
				try {
					huc.getOutputStream().write(body.getBytes("utf8"));
					if ((throwable = readResponse(huc, response)) != null) {
						l.onError(HighscoresTableImpl.this, throwable);
					} else {
						try {
							if (response.transportStatusCode == HttpURLConnection.HTTP_OK) {
								response.content = new AddEntryResponseContent(
										JSONValue.parse(response.responseBody));
							}
							l.onResponse(HighscoresTableImpl.this, response);
						} catch (Throwable t) {
							l.onError(HighscoresTableImpl.this, t);
						}
					}
				} catch (Throwable t) {
					t.printStackTrace(cfg.getPrintStream());
				}
			}
		};
		thread.start();
		return new RequestHandleImpl<AddEntryResponse>(response, thread);
	}

	public RequestHandle<CheckEntryResponse> doCheckEntry(
			final HighscoreRecord hr,
			final ResponseListener<CheckEntryResponse> l)
			throws HighscoresTableException {
		HashMap<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("score", Double.toString(hr.getScore()));
		String playerName = hr.getBy_whom();
		if (playerName != null) {
			queryParams.put("by_whom", playerName);
		}
		final HttpURLConnection huc = prepareConnection("GET", "", "/check",
				queryParams);
		final CheckEntryResponseImpl response = new CheckEntryResponseImpl();
		final Thread thread = new Thread() {
			public void run() {
				Throwable throwable;
				try {
					if ((throwable = readResponse(huc, response)) != null) {
						l.onError(HighscoresTableImpl.this, throwable);
					} else {
						try {
							if (response.transportStatusCode == HttpURLConnection.HTTP_OK) {
								response.content = new CheckEntryResponseContent(
										JSONValue.parse(response.responseBody));
							}
							l.onResponse(HighscoresTableImpl.this, response);
						} catch (Throwable t) {
							l.onError(HighscoresTableImpl.this, t);
						}
					}
				} catch (Throwable t) {
					t.printStackTrace(cfg.getPrintStream());
				}
			}
		};
		thread.start();
		return new RequestHandleImpl<CheckEntryResponse>(response, thread);
	}

	/**
	 * Read the server response
	 * 
	 * @param huc
	 * @param response
	 * @return an instance of {@link Throwable} if there is any error
	 */
	private static Throwable readResponse(final HttpURLConnection huc,
			final CommonResponseImpl response) {
		try {
			response.transportStatusCode = huc.getResponseCode();
			response.transportExtraMessage = huc.getResponseMessage();
			InputStream is = response.transportStatusCode == HttpURLConnection.HTTP_OK ? huc
					.getInputStream() : huc.getErrorStream();
			final String s = readFully(is);
			response.responseBody = s;
		} catch (Throwable t) {
			return t;
		} finally {
			huc.disconnect();
		}
		return null;
	}
}