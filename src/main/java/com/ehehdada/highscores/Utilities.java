package com.ehehdada.highscores;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 
 * <p>
 * Description:
 * </p>
 * <p>
 * Date: 2014/7/24
 * </p>
 * <p>
 * Copyright (C) 2014 ehehdada, ltd. (<a
 * href="https://www.ehehdada.com">https://www.ehehdada.com</a>) - The ehehdada,
 * ltd. team (<a href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * </p>
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <a
 * href="http://www.gnu.org/licenses/lgpl.html">http
 * ://www.gnu.org/licenses/lgpl.html</a>.
 * </p>
 * 
 * @author The ehehdada, ltd. team (<a
 *         href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * @since 1.0
 */
final class Utilities {

	private Utilities() {
	}

	static byte[] hexStringToByteArray(String hexString) {
		char[] ca = hexString.toCharArray();
		final int baLength = ca.length >> 1;
		byte[] ba = new byte[baLength];
		for (int i = 0, j = 0; i < baLength; i++, j += 2) {
			ba[i] = (byte) ((Utilities.hexCharToByte(ca[j]) << 4) | Utilities
					.hexCharToByte(ca[j + 1]));
		}
		return ba;
	}

	static int hexCharToByte(char c) {
		if (c >= 'a' && c <= 'f') {
			return c - 'a' + 10;
		}
		return c - '0';
	}

	static String byteArrayToHexString(byte[] ba) {
		final int baLength = ba.length;
		StringBuffer sb = new StringBuffer(baLength >> 1);
		for (int i = 0; i < baLength; i++) {
			int b = ba[i] & 0xff;
			sb.append(nibbleToHexChar(b >> 4)).append(nibbleToHexChar(b & 0xf));
		}
		return sb.toString();
	}

	static char nibbleToHexChar(int b) {
		if (b >= 0xa) {
			return (char) ('a' + b - 0xa);
		}
		return (char) ('0' + b);
	}

	/**
	 * Calculates the finger print for the given parameters
	 * 
	 * @param uri
	 * @param requestBody
	 * @param timestamp
	 * @param tableKey
	 * @param hashedSharedSecret
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	static String getFingerprint(String uri, String requestBody,
			long timestamp, String tableKey, String hashedSharedSecret)
			throws NoSuchAlgorithmException {
		final MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.reset();
		final StringBuffer stringToHash = new StringBuffer().append(uri)
				.append('|').append(requestBody).append('|').append(timestamp)
				.append('|').append(tableKey).append('|')
				.append(hashedSharedSecret);
		final int length = stringToHash.length();
		final String s = stringToHash.append('|').append(length).toString();
		return Utilities.byteArrayToHexString(md.digest(s.getBytes()));
	}

}
