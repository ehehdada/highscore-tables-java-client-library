/**
 * 
 */
package com.ehehdada.highscores;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.logging.Logger;

import org.junit.Test;

/**
 * <p>
 * Description: Tests for 'add entries' to the high score tables
 * </p>
 * <p>
 * Date: 2014/7/26
 * </p>
 * <p>
 * Copyright (C) 2014 ehehdada, ltd. (<a
 * href="https://www.ehehdada.com">https://www.ehehdada.com</a>) - The ehehdada,
 * ltd. team (<a href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * </p>
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <a
 * href="http://www.gnu.org/licenses/lgpl.html">http
 * ://www.gnu.org/licenses/lgpl.html</a>.
 * </p>
 * 
 * @author The ehehdada, ltd. team (<a
 *         href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * @since 1.0
 */
public class AddEntriesHighscoresTest {

	static Logger logger = Logger.getLogger(AddEntriesHighscoresTest.class
			.getName());

	@Test
	public void testAddHighscoreTest() throws NoSuchAlgorithmException,
			URISyntaxException, HighscoresTableException, InterruptedException,
			CertificateException, KeyStoreException, IOException,
			KeyManagementException {
		testAddHighscore(new TestConfiguration());
	}

	@Test
	public void testAddHighscoreTest2() throws NoSuchAlgorithmException,
			URISyntaxException, HighscoresTableException, InterruptedException,
			CertificateException, KeyStoreException, IOException,
			KeyManagementException {
		testAddHighscore(new Test2Configuration());
	}

	private static void testAddHighscore(Configuration cfg)
			throws NoSuchAlgorithmException, URISyntaxException,
			HighscoresTableException, InterruptedException,
			CertificateException, KeyStoreException, IOException,
			KeyManagementException {
		HighscoresTable ht = HighscoresTableFactory.getInstance(cfg);
		final Object monitor = new Object();
		logger.info("before request delete");
		RequestHandle<ResetTableResponse> rhrtr = ht
				.doResetTable(new ResponseListener<ResetTableResponse>() {

					public void onResponse(HighscoresTable ht,
							ResetTableResponse response) {
						logger.info("delete completed!");
						try {
							logger.info("delete status = "
									+ response.getTransportStatusCode()
									+ ", extra message = "
									+ response.getTransportExtraMessage());
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						synchronized (monitor) {
							monitor.notify();
						}
					}

					public void onError(HighscoresTable ht, Throwable t) {
						logger.severe("error " + t);
						t.printStackTrace();
						synchronized (monitor) {
							monitor.notify();
						}
					}
				});
		if (!rhrtr.isDone()) {
			synchronized (monitor) {
				logger.info("before wait ResetTableResponse");
				monitor.wait();
			}
		}
		RequestHandle<AddEntryResponse> rhgtr = ht.doAddEntry(
				new HighscoreRecordImpl(),
				new ResponseListener<AddEntryResponse>() {

					public void onResponse(HighscoresTable ht,
							AddEntryResponse response) {
						logger.info("add entry completed!");
						try {
							logger.info("add entry status = "
									+ response.getTransportStatusCode()
									+ ", extra message = "
									+ response.getTransportExtraMessage()
									+ ", entry limit = "
									+ response.getAddEntryResponseContent()
											.getEntry_limit()
									+ ", position = "
									+ response.getAddEntryResponseContent()
											.getPosition()
									+ ", ip6 = "
									+ response.getAddEntryResponseContent()
											.getIp6()
									+ ", ip4 = "
									+ response.getAddEntryResponseContent()
											.getIp4());
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						logger.info("response body: "
								+ response.getResponseBody());
						synchronized (monitor) {
							monitor.notify();
						}
					}

					public void onError(HighscoresTable ht, Throwable t) {
						logger.severe("error " + t);
						t.printStackTrace();
						synchronized (monitor) {
							monitor.notify();
						}
					}
				});
		if (!rhgtr.isDone()) {
			synchronized (monitor) {
				logger.info("before wait AddEntryResponse");
				monitor.wait();
			}
		}
	}

}
