/**
 * 
 */
package com.ehehdada.highscores;

import static org.junit.Assert.*;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;

/**
 * <p>
 * Description: testing some utilities
 * </p>
 * <p>
 * Date: 2014/7/24
 * </p>
 * <p>
 * Copyright (C) 2014 ehehdada, ltd. (<a
 * href="https://www.ehehdada.com">https://www.ehehdada.com</a>) - The ehehdada, ltd. team
 *  (<a
 * href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * </p>
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <a
 * href="http://www.gnu.org/licenses/lgpl.html">http
 * ://www.gnu.org/licenses/lgpl.html</a>.
 * </p>
 * 
 * @author The ehehdada, ltd. team  (<a
 *         href="mailto:support@ehehdada.com">support@ehehdada.com</a>)
 * @since 1.0
 */
public class UtilitiesTest {

	@Test
	public void testHexCharToByteLetter() {
		assertEquals(0xe, Utilities.hexCharToByte('e'));
	}

	@Test
	public void testHexCharToByteDigit() {
		assertEquals(0x7, Utilities.hexCharToByte('7'));
	}

	@Test
	public void testHexStringToByteArray() {
		assertArrayEquals(new byte[] { 0x12, 0x34 },
				Utilities.hexStringToByteArray("1234"));
	}

	@Test
	public void testNibbleToCharLetter() {
		assertEquals('e', Utilities.nibbleToHexChar(0xe));
	}

	@Test
	public void testNibbleToCharDigit() {
		assertEquals('2', Utilities.nibbleToHexChar(0x2));
	}

	@Test
	public void testByteArrayToHexString() {
		assertEquals("1234",
				Utilities.byteArrayToHexString(new byte[] { 0x12, 0x34 }));
	}

	@Test
	public void testFingerprint_NoBody_Timestamp0()
			throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.reset();
		assertEquals(
				"11612a31505f82da1c57f8b9339be79ba5ad50c43a5ece8154c92454322bfdeb",
				Utilities.getFingerprint("/api/test", "", 0L, "test", Utilities
						.byteArrayToHexString(md.digest("test".getBytes()))));
	}

	@Test
	public void testFingerprint_Check() throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.reset();
		assertEquals(
				"b0a7c509742833b5179ee8f8b59e26403d10292f7b8f2224ad0b21438ee80db3",
				Utilities.getFingerprint(
						"/api/test/check?score=123&by_whom=player#5", "",
						1407254165107L, "test", Utilities
								.byteArrayToHexString(md.digest("test"
										.getBytes()))));
	}

	@Test
	public void testFingerprint_Delete() throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.reset();
		assertEquals(
				"e0153c896f3f565466192bc2bd6d915de60d0b21ce263b867522b70e4eae738c",
				Utilities.getFingerprint("/api/test", "", 1407254300102L,
						"test", Utilities.byteArrayToHexString(md.digest("test"
								.getBytes()))));
	}

	@Test
	public void testFingerprint_Post() throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.reset();
		assertEquals(
				"671c69bebb3621fcaed6c0d46c3790901f7e79f670411924d1d39fd6ea9499b5",
				Utilities
						.getFingerprint(
								"/api/test",
								"{\"score\":\"123\",\"by_whom\":\"player#5\",\"when_happened\":1407254637072,\"locale\":\"en-US\"}",
								1407254636090L, "test", Utilities
										.byteArrayToHexString(md.digest("test"
												.getBytes()))));
	}
}
